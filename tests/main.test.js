import TokenHelper from '../src/classes/TokenHelper';

import chai from 'chai';

let assert = chai.assert;

describe('Класс TokenHelper()', function () {

    let classInstance;

    before(() => classInstance = new TokenHelper());

    after(() => classInstance.removeTokens());

    describe('Метод setToken()', () => {

        afterEach(() => {
           classInstance.removeTokens();
        });

        it('Передаем нормальные параметры для создания токена', () => {
            let token_access = 'fdjshfjk435439yrt43jllkj34';
            let expires_in = 3600 * 2;
            let refresh_token = 'gduytrpyioprtynmrtnytr344v24324n4mmgf';
            let token_type = 'Bearear';

            const result = classInstance.setToken(token_access, expires_in, token_type, refresh_token);
            assert.isTrue(result);
        });

        describe('Параметр конструктора "access_token"', () => {

            afterEach(() => classInstance.removeTokens());

            it('Обработка EMPTY "access_token"', () => {
                let token_access = '';
                let expires_in = 5000;
                let refresh_token = 'fdsfdsfsdfsdf';
                let token_type = 'Bearear';

                const result = classInstance.setToken(token_access, expires_in, token_type, refresh_token);
                assert.instanceOf(result, Error);
            });

            it('Обработка NUMBER "access_token"', () => {
                let token_access = 53453;
                let expires_in = 5000;
                let refresh_token = 'fdsfdsfsdfsdf';
                let token_type = 43434;

                const result = classInstance.setToken(token_access, expires_in, token_type, refresh_token);
                assert.instanceOf(result, Error);
            });

            it('Обработка UNDEFINED "access_token"', () => {
                let token_access = undefined;
                let expires_in = 5000;
                let refresh_token = 'fdsfdsfsdfsdf';
                let token_type = undefined;

                const result = classInstance.setToken(token_access, expires_in, token_type, refresh_token);
                assert.instanceOf(result, Error);
            });

            it('Обработка NULL "access_token"', () => {
                let token_access = null;
                let expires_in = 5000;
                let refresh_token = 'fdsfdsfsdfsdf';
                let token_type = null;

                const result = classInstance.setToken(token_access, expires_in, token_type, refresh_token);
                assert.instanceOf(result, Error);
            });
        });


        it('Обработка некорректного времени жизни для токена', () => {
            let token_access = 'fdjshfjk435439yrt43jllkj34';
            let expires_in = 'fsdf';
            let refresh_token = 'gduytrpyioprtynmrtnytr344v24324n4mmgf';
            let token_type = 'Bearear';

            const result = classInstance.setToken(token_access, expires_in, token_type, refresh_token);
            assert.instanceOf(result, Error);
        });

        describe('Параметр конструктора "token_type"', () => {

            afterEach(() => classInstance.removeTokens());

            it('Обработка EMPTY "token_type"', () => {
                let token_access = 'fdjshfjk435439yrt43jllkj34';
                let expires_in = 5000;
                let refresh_token = 'fdsfdsfsdfsdf';
                let token_type = '';

                const result = classInstance.setToken(token_access, expires_in, token_type, refresh_token);
                assert.instanceOf(result, Error);
            });

            it('Обработка NUMBER "token_type"', () => {
                let token_access = 'fdjshfjk435439yrt43jllkj34';
                let expires_in = 5000;
                let refresh_token = 'fdsfdsfsdfsdf';
                let token_type = 43434;

                const result = classInstance.setToken(token_access, expires_in, token_type, refresh_token);
                assert.instanceOf(result, Error);
            });

            it('Обработка UNDEFINED "token_type"', () => {
                let token_access = 'fdjshfjk435439yrt43jllkj34';
                let expires_in = 5000;
                let refresh_token = 'fdsfdsfsdfsdf';
                let token_type = undefined;

                const result = classInstance.setToken(token_access, expires_in, token_type, refresh_token);
                assert.isTrue(result);
            });

            it('Обработка NULL "token_type"', () => {
                let token_access = 'fdjshfjk435439yrt43jllkj34';
                let expires_in = 5000;
                let refresh_token = 'fdsfdsfsdfsdf';
                let token_type = null;

                const result = classInstance.setToken(token_access, expires_in, token_type, refresh_token);
                assert.instanceOf(result, Error);
            });
        });

        describe('Параметр конструктора "token_type"', () => {

            afterEach(() => classInstance.removeTokens());

            it('Обработка EMPTY "refresh_token"', () => {
                let token_access = 'fdjshfjk435439yrt43jllkj34';
                let expires_in = 5000;
                let refresh_token = '';
                let token_type = 'Bearear';

                const result = classInstance.setToken(token_access, expires_in, token_type, refresh_token);
                assert.instanceOf(result, Error);
            });

            it('Обработка NUMBER "refresh_token"', () => {
                let token_access = 'fdjshfjk435439yrt43jllkj34';
                let expires_in = 5000;
                let refresh_token = 543534;
                let token_type = 'Bearear';

                const result = classInstance.setToken(token_access, expires_in, token_type, refresh_token);
                assert.instanceOf(result, Error);
            });

            it('Обработка UNDEFINED "refresh_token"', () => {
                let token_access = 'fdjshfjk435439yrt43jllkj34';
                let expires_in = 5000;
                let refresh_token = undefined;
                let token_type = 'Bearear';

                const result = classInstance.setToken(token_access, expires_in, token_type, refresh_token);
                assert.isTrue(result);
            });

            it('Обработка NULL "refresh_token"', () => {
                let token_access = 'fdjshfjk435439yrt43jllkj34';
                let expires_in = 5000;
                let refresh_token = null;
                let token_type = 'Bearear';

                const result = classInstance.setToken(token_access, expires_in, token_type, refresh_token);
                assert.instanceOf(result, Error);
            });
        });
    });

    describe('Метод setTokenExpire()', () => {
        it('Утсанавливаем корректное время токена с запасом', () => {
            const token_lifetime = 5000;
            assert.isTrue(classInstance.setTokenExpire(token_lifetime));
            assert.isFalse(classInstance.tokenWasExpired());
        });

        it('Обработка установки просроченного времени жизни токена', () => {
            const token_lifetime = -5000;
            assert.isTrue(classInstance.setTokenExpire(token_lifetime));
            assert.isTrue(classInstance.tokenWasExpired());
        });

        it('Обработка STRING "expires_in"', () =>{
            const token_lifetime = 'some string';
            assert.instanceOf(classInstance.setTokenExpire(token_lifetime), Error);
        });

        it('Обработка EMPTY "expires_in"', () =>{
            const token_lifetime = '';
            assert.instanceOf(classInstance.setTokenExpire(token_lifetime), Error);
        });

        it('Обработка UNDEFINED "expires_in"', () =>{
            const token_lifetime = undefined;
            assert.instanceOf(classInstance.setTokenExpire(token_lifetime), Error);
        });

        it('Обработка NULL "expires_in"', () =>{
            const token_lifetime = null;
            assert.instanceOf(classInstance.setTokenExpire(token_lifetime), Error);
        });
    });

    describe('Метод isAuthenticated()', () => {

        beforeEach(() => classInstance.removeTokens());

        it('Удачная авторизация', () => {
            let token_access = 'fdjshfjk435439yrt43jllkj34';
            let expires_in = 3600 * 2;
            let refresh_token = 'gduytrpyioprtynmrtnytr344v24324n4mmgf';
            let token_type = 'Bearear';

            const result = classInstance.setToken(token_access, expires_in, token_type, refresh_token);
            assert.isTrue(result);

            assert.isTrue(classInstance.isAuthenticated());
        });

        it('Неудачная авторизация', () => {
            let token_access = 'fdjshfjk435439yrt43jllkj34';
            let expires_in = -5000;
            let refresh_token = 'gduytrpyioprtynmrtnytr344v24324n4mmgf';
            let token_type = 'Bearear';

            const result = classInstance.setToken(token_access, expires_in, token_type, refresh_token);
            assert.isTrue(result);

            assert.isFalse(classInstance.isAuthenticated());
        })
    });

    describe('Метод authUser()', () => {

        describe('Входной параметр "client_id"', () => {
            it('Обработка STRING "client_id"', () => {
                let client_id = '43243',
                    client_secret = 'sdasf23r423rfsd',
                    grantType = 'password',
                    scope = '*';

                const result = classInstance.authUser(client_id, client_secret, grantType, scope);
                assert.instanceOf(result, Error);
            });

            it('Обработка EMPTY "client_id"', () => {
                let client_id = '',
                    client_secret = 'sdasf23r423rfsd',
                    grantType = 'password',
                    scope = '*';

                const result = classInstance.authUser(client_id, client_secret, grantType, scope);
                assert.instanceOf(result, Error);
            });

            it('Обработка UNDEFINED "client_id"', () => {
                let client_id = undefined,
                    client_secret = 'sdasf23r423rfsd',
                    grantType = 'password',
                    scope = '*';

                const result = classInstance.authUser(client_id, client_secret, grantType, scope);
                assert.instanceOf(result, Error);
            });

            it('Обработка NULL "client_id"', () => {
                let client_id = null,
                    client_secret = 'sdasf23r423rfsd',
                    grantType = 'password',
                    scope = '*';

                const result = classInstance.authUser(client_id, client_secret, grantType, scope);
                assert.instanceOf(result, Error);
            });
        });

        describe('Входной параметр "client_secret"', () => {
            it('Обработка NUMBER "client_secret"', () => {
                let client_id = 2,
                    client_secret = 4343,
                    grantType = 'password',
                    scope = '*';

                const result = classInstance.authUser(client_id, client_secret, grantType, scope);
                assert.instanceOf(result, Error);
            });

            it('Обработка EMPTY "client_secret"', () => {
                let client_id = 2,
                    client_secret = '',
                    grantType = 'password',
                    scope = '*';

                const result = classInstance.authUser(client_id, client_secret, grantType, scope);
                assert.instanceOf(result, Error);
            });

            it('Обработка UNDEFINED "client_secret"', () => {
                let client_id = 2,
                    client_secret = undefined,
                    grantType = 'password',
                    scope = '*';

                const result = classInstance.authUser(client_id, client_secret, grantType, scope);
                assert.instanceOf(result, Error);
            });

            it('Обработка NULL "client_secret"', () => {
                let client_id = 2,
                    client_secret = null,
                    grantType = 'password',
                    scope = '*';

                const result = classInstance.authUser(client_id, client_secret, grantType, scope);
                assert.instanceOf(result, Error);
            });
        })

        describe('Входной параметр "scope"', () => {
            it('Обработка NUMBER "scope"', () => {
                let client_id = 2,
                    client_secret = 'dsadsa',
                    grantType = 'password',
                    scope = 434;

                const result = classInstance.authUser(client_id, client_secret, grantType, scope);
                assert.instanceOf(result, Error);
            });

            it('Обработка EMPTY "scope"', () => {
                let client_id = 2,
                    client_secret = 'dasdasdasd',
                    grantType = 'password',
                    scope = '';

                const result = classInstance.authUser(client_id, client_secret, grantType, scope);
                assert.instanceOf(result, Error);
            });

            it('Обработка UNDEFINED "scope"', () => {
                let client_id = 2,
                    client_secret = 'fsdfsdfsdf',
                    grantType = 'password',
                    scope = undefined;

                const result = classInstance.authUser(client_id, client_secret, grantType, scope);
                assert.isTrue(result);
            });

            it('Обработка NULL "scope"', () => {
                let client_id = 2,
                    client_secret = 'fdsfdf',
                    grantType = 'password',
                    scope = null;

                const result = classInstance.authUser(client_id, client_secret, grantType, scope);
                assert.instanceOf(result, Error);
            });
        })
    })

});