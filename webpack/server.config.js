const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = function() {
    return {
        devServer: {
            stats: 'errors-only'
        },
        plugins: [new HtmlWebpackPlugin()]
    }
};