module.exports = function() {
    return {
        module: {
            rules: [{
                test: /\.test\.js$/,
                use: 'mocha-loader',
                exclude: /node_modules/,
            }]
        }
    }
};