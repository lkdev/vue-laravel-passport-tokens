const path = require('path');
const merge = require('webpack-merge');
const MinifyPlugin = require("babel-minify-webpack-plugin");

const testsConfig = require('./webpack/tests.config');
const serverConfig = require('./webpack/server.config');

const PATHS = {
    source: path.resolve(__dirname, 'src'),
    build: path.resolve(__dirname, 'lib'),
    tests: path.resolve(__dirname, 'tests')
};

const common = {
    entry: PATHS.source + '/index.ts',
    output: {
        path: PATHS.build,
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    devtool: 'inline-source-map',
    resolve: {
        extensions: ['.ts', '.js']
    }
};

module.exports = function (env) {
    switch (env) {

        case 'production':
            return merge([
                common,
                {
                    plugins: [
                        new MinifyPlugin()
                    ]
                }
            ]);

        case 'development':
            return merge([
                common,
                serverConfig()
            ]);

        case 'tests':
            common.entry = PATHS.tests + '/main.test.js';

            return merge([
                common,
                testsConfig(),
                serverConfig()
            ]);

        default:
            return common
    }
};